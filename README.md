## Back in History Game
- Serious Game course project at CentraleSupelec 
- 2D card game created to aid students in history classes / general culture game 
- Implemented using **Unity and C#**
- Demo video in the repo ***BackinHistoryDemo.mp4***
