using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneChanger : MonoBehaviour
{
    public static string selectedTheme = "General Culture";
    public static int score = 0;
    public void gameScene()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void mainScene()
    {
        SceneManager.LoadScene("FirstScene");
    }

    public void endScene(int nb)
    {
        score = nb;
        SceneManager.LoadScene("EndScene");
    }
    public void changeTheme(string theme)
    {
        selectedTheme = theme;
    }
}