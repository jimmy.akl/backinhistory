using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
	private string theme = SceneChanger.selectedTheme;
	private int score = 0;
	public int addScoreValue;
	public int removeScoreValue;
	public TextMeshProUGUI scoretext;

	public List<GameObject> dateSlots;
	public float snapDistance =2.0f;

	public List<Card> deck;
	public List<Card> presidentdeck;
	public TextMeshProUGUI deckSizeText;

	public Transform[] cardSlots;
	public bool[] availableCardSlots;

	public List<Card> discardPile;
	public TextMeshProUGUI discardPileSizeText;

	private Animator camAnim;
	public SceneChanger sc;

	//singleton
	public static GameManager _gm;
	public static GameManager gm
	{
		get
		{
			if (_gm == null)
			{
				_gm = GameObject.FindObjectOfType<GameManager>();
			}
			return _gm;
		}
	}
	private void Start()
	{
		camAnim = Camera.main.GetComponent<Animator>();
		if (theme == "French Presidents")
		{
			deck = presidentdeck;
		}
	}

	public void DrawCards()
	{
		if (deck.Count >= 1 && roundComplete())
		{
			List<GameObject> dateSlotsClone = new List<GameObject>(dateSlots);
			Card randomCard;
			GameObject randomSlot;

			for (int i = 0; i < availableCardSlots.Length; i++)
			{ 
				randomCard = deck[Random.Range(0, deck.Count)];
				randomSlot = dateSlotsClone[Random.Range(0, dateSlotsClone.Count)];

				if (availableCardSlots[i] == true)
				{
					randomCard.gameObject.SetActive(true);
					camAnim.SetTrigger("shake");
					randomCard.handIndex = i;
					randomCard.transform.position = new Vector3(cardSlots[i].position.x, cardSlots[i].position.y, cardSlots[i].position.z);
					randomCard.initialPos = randomCard.transform.position;
					randomCard.hasBeenPlayed = false;
					deck.Remove(randomCard);
					availableCardSlots[i] = false;

					Transform year = randomSlot.transform.Find("year");
					year.gameObject.GetComponent<TextMesh>().text = randomCard.year.ToString();
					dateSlotsClone.Remove(randomSlot);
					//return;
				}
			}
		}
	}

	public void Shuffle()
	{/*
		if (discardPile.Count >= 1)
		{
			foreach (Card card in discardPile)
			{
				deck.Add(card);
			}
			discardPile.Clear();
		}*/
	}

	private void Update()
	{
		deckSizeText.text = deck.Count.ToString();
		discardPileSizeText.text = discardPile.Count.ToString();
		scoretext.text = score.ToString();
	}

	public bool roundComplete() {
		foreach (bool slot in availableCardSlots) {
			if (!slot) return false;
		}
		return true;
	}

	public void gameComplete()
    {
		if (deck.Count == 0 && roundComplete())
		{
			sc.endScene(score);
			
		}
    }

	public void addScore() 
	{
		score += addScoreValue;
	}

	public void removeScore() 
	{
		score -= removeScoreValue;
	}
	
}
