using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Card : MonoBehaviour
{
	public int year;
	public bool hasBeenPlayed;
	public int handIndex;
	public string description;
	public string title;

	GameManager gm;

	private Animator anim;
	private Animator camAnim;

	public GameObject effect;
	public GameObject hollowCircle;
	public GameObject descView;

	public Vector3 initialPos;
	private Vector3 screenPoint;
	private Vector3 offset;

	private void Start()
	{
		gm = GameManager.gm;
		anim = GetComponent<Animator>();
		camAnim = Camera.main.GetComponent<Animator>();
		initialPos = transform.position;
	}
	private void OnMouseDown()
	{
		if (!descView.activeSelf)
		{
			if (!hasBeenPlayed)
			{
				Instantiate(hollowCircle, transform.position, Quaternion.identity);
				screenPoint = Camera.main.WorldToScreenPoint(transform.position);
				offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
			}
		}
	}

    private void OnMouseDrag()
    {
		if (!descView.activeSelf)
		{
			if (!hasBeenPlayed)
			{
				Vector3 currentScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
				Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenPoint) + offset;

				transform.position = currentPosition;
			}
		}
	}

    private void OnMouseUp()
    {
		if (!descView.activeSelf)
		{
			if (!hasBeenPlayed)
			{
				foreach (GameObject dateslot in gm.dateSlots)
				{
					if (Vector3.Distance(dateslot.transform.position, transform.position) < gm.snapDistance && dateslot.transform.Find("year").GetComponent<TextMesh>().text == year.ToString())
					{
						transform.position = dateslot.transform.position;
						hasBeenPlayed = true;
						gm.availableCardSlots[handIndex] = true;
						Instantiate(effect, transform.position, Quaternion.identity);
						gm.addScore();
						gm.discardPile.Add(this);
						Invoke("showDesc", 1f);
						return;
					}
				}
				gm.removeScore();
				transform.position = initialPos;
				camAnim.SetTrigger("shake");
				anim.SetTrigger("move");
			}
		}
	}

    void MoveToDiscardPile()
	{
		//Instantiate(effect, transform.position, Quaternion.identity);
		gameObject.SetActive(false);
	}

    private void Update()
    {
        if(gm.roundComplete()) Invoke("MoveToDiscardPile", 2f);
	}

	private void showDesc()
	{
		descView.SetActive(true);
		Transform titlegm = descView.transform.Find("Title");
		titlegm.gameObject.GetComponent<TextMeshProUGUI>().text = title;

		Transform desc = descView.transform.Find("desctxt");
		desc.gameObject.GetComponent<TextMeshProUGUI>().text = description;
	}
}


// wanted people to improve their game's gameplay by learning tricks in game feel