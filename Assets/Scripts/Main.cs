using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour
{
    // Singleton
    private static Main _instance;
    public static Main instance
    {
        get{
            if (_instance == null) {
                _instance = GameObject.FindObjectOfType<Main>();
            }
            return _instance;
        }

    }
    public void gameOver() {
        Debug.Log("Game Over");
    }
}